defmodule CheetahApiWeb.FallbackController do
    use CheetahApiWeb, :controller
    alias CheetahApiWeb.{ChangesetView, ErrorView}
    alias Ecto.Changeset

    @type supported_fallbacks :: {:error, Changeset.t} |
                                 {:error, :not_authorized} |
                                 {:error, :not_found} |
                                 nil

    @spec call(Conn.t, supported_fallbacks) ::  Conn.t
    def call(%Conn{} = conn, {:error, %Changeset{} = changeset}) do
      conn
      |> put_status(:unprocessable_entity)
      |> render(ChangesetView, "422.json", changeset: changeset)
    end
    def call(%Conn{} = conn, {:error, :not_authorized}) do
      conn
      |> put_status(:unauthorized)
      |> render(ErrorView, "401.json")
    end
    def call(%Conn{} = conn, {:error, :not_found}) do
      conn
      |> put_status(:not_found)
      |> render(ErrorView, "404.json")
    end
end
