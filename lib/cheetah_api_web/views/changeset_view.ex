defmodule CheetahApiWeb.ChangesetView do
  use CheetahApiWeb, :view

  alias Ecto.Changeset

  def render("422.json", %{changeset: changeset}) do
    %{
      jsonapi: %{
        version: "1.0"
      },
      errors: translate_errors(changeset)
    }
  end

  def translate_errors(%Changeset{ errors: errors}) do
    errors
    |> Enum.reduce(%{},fn({field, {msg, _}}, acc) ->
       Map.update(acc,field,msg,&(&1))end)
  end

end
