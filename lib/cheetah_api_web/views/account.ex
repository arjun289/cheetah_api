defmodule CheetahApiWeb.AccountView do
  use CheetahApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [:id, :first_name, :last_name, :email_address, :profile_pic_url]

end
