defmodule CheetahApi.UserProfile.CorrespondenceAddress do
  use CheetahApi.Data

  alias CheetahApi.UserProfile.ContactDetail

  schema "correspondence_addresses" do
    field :address, :string
    field :locality, :string
    field :city, :string
    field :postal_code, :string

    belongs_to :contact_detail, ContactDetail

    timestamps()
  end

end