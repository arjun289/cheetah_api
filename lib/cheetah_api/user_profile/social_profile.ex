defmodule CheetahApi.UserProfile.SocialProfile do
  use CheetahApi.Data

  alias CheetahApi.UserProfile.User

  schema "social_profiles" do
    field :name
    field :url

    belongs_to :user, User

    timestamps()
  end

end