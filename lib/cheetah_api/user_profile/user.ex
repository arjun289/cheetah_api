defmodule CheetahApi.UserProfile.User do
  use CheetahApi.Data

  alias CheetahApi.UserProfile.{User, ContactDetail, SocialProfile}
  alias CheetahApi.ProfessionalDetails.{WorkExperience}

  schema "users" do
    field :email_address, :string
    field :gender, :string
    field :dob, :date
    field :mobile_number, :string
    field :current_city, :string
    field :profile_pic_url, :string
    field :first_name, :string
    field :last_name, :string

    has_one :contact_detail, ContactDetail
    has_one :work_experience, WorkExperience

    has_many :socail_profiles, SocialProfile

    timestamps()
  end

  def changeset(struct, params\\ %{}) do
    fields = [:first_name, :last_name, :email_address, :gender, :dob, :mobile_number, :current_city]
    struct
      |> cast(params, fields)
      |> validate_required(fields)
  end

end
