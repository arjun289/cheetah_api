defmodule CheetahApi.Courses.Course do
  use CheetahApi.Data
  alias CheetahApi.Courses.{Course, Stream}


  schema "courses" do
    field :duration, :integer
    field :name, :string

    has_many :streams, Stream
  end
  @doc false
  def changeset(%Course{} = course, attrs) do
    course
    |> cast(attrs, [:name, :duration])
    |> validate_required([:name, :duration])
  end
end
