defmodule CheetahApi.Courses.Stream do
  use Ecto.Schema
  import Ecto.Changeset
  alias CheetahApi.Courses.{Stream, Batch, Course}


  schema "streams" do
    field :name, :string

    belongs_to :course, Course

    has_many :batches, Batch
    timestamps()
  end

  @doc false
  def changeset(%Stream{} = stream, attrs) do
    stream
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
