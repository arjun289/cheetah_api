defmodule CheetahApi.Repo.Migrations.CreateSocialProfiles do
  use Ecto.Migration

  def change do
    create table(:social_profiles) do
      add :name, :string
      add :url, :string

      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

    create index(:social_profiles, [:user_id])
  end
end
