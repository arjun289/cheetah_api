defmodule CheetahApi.Repo.Migrations.CreateWorkExperiences do
  use Ecto.Migration

  def change do
    create table(:work_experiences) do
      add :professional_headline, :text
      add :total_years_experience, :integer
      
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end
  end
end
