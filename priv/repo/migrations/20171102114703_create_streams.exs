defmodule CheetahApi.Repo.Migrations.CreateStream do
  use Ecto.Migration

  def change do
    create table(:streams) do
      add :name, :string

      add :course_id, references(:courses, on_delete: :delete_all)
      timestamps()
    end

  end
end
