# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :cheetah_api,
  ecto_repos: [CheetahApi.Repo]

# Configures the endpoint
config :cheetah_api, CheetahApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "JHeqwvXlSrQVAvpq/42bnDmni1T3MhrXUjFj7z1vJgX+5nVi/Y/0Do1HQQSLoype",
  render_errors: [view: CheetahApiWeb.ErrorView, accepts: ~w(json json-api)],
  pubsub: [name: CheetahApi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configures Guardian library
config :guardian, Guardian,
  allowed_algos: ["HS512"],
  verify_module: Guardian.JWT,
  issuer: "Aviabird",
  ttl: {30, :days},
  verify_issuer: true,
  secret_key: "mrOI5BUw3rUI/n4NuIW5ztOjRyp1t0/deDCCP1xfwDEJYsoq13XFN1DPNiAGUsp0",
  serializer: CheetahApiWeb.GuardianSerializer

# Configures JSON API encoding
config :phoenix, :format_encoders,
  "json-api": Poison

# Configures JSON API mime type
config :mime, :types, %{
  "application/vnd.api+json" => ["json-api"]
}

# Configures Key Format
config :ja_serializer,
  key_format: {:custom, CheetahApi.StringFormatter, :camelize}

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
